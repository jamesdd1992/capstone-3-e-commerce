
import {useState, useEffect, useContext} from 'react'
import UserContext from '../UserContext';
import { Container, Form, Button } from 'react-bootstrap';
import { Navigate } from 'react-router-dom';
import swal from 'sweetalert2'
import AppNavBar from '../components/AppNavBar';


export default function Register(){
     let {user} = useContext(UserContext);
    const [userName, setUserName] = useState('');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [middleName, setMiddleName] = useState('');
    const [address, setAddress] = useState('');
    const [email, setEmail] = useState('');
    const [mobileNo, setMobileNo] = useState('');
    const [password1, setPassword1] = useState('');
    const [password2, setPassword2] = useState('');
    const [isActive, setIsActive] = useState(false);
    const [isMatched, setIsMatched] = useState(false);
    const [isMobileValid, setIsMobileValid] = useState(false)
    const [isAllowed, setIsAllowed] = useState(false)

    useEffect(() => {
        if (mobileNo.length === 11 ) {
            if (
                password1 === password2 && 
                password1 !== '' && password2 !== ''
            ) {
                setIsMatched(true);
                if (firstName !== '' && lastName !== '' && email !== '' && userName !== '' && address !== '' &&middleName !== '')  {
                    setIsActive(true)
                    setIsAllowed(true)
                } else {
                    setIsActive(false)
                    setIsAllowed(false)
                }
            } else {
                setIsMatched(false)
            }
             setIsMobileValid(true)
            
        }else if(password1 !== '' && password1 === password2){
            setIsMatched(true)
        }else {   
            setIsMobileValid(false)
            setIsMatched(false)
            setIsAllowed(false)
            setIsActive(false)   
            }
    },[firstName, lastName, email, password1, password2, mobileNo, userName, address, middleName]);

    const registerUser = async (eventSubmit) => {
        eventSubmit.preventDefault()

        const isRegistered = await fetch('https://capstone2-batch156.herokuapp.com/users/register/', {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
                userName: userName,
				firstName: firstName,
				lastName: lastName,
                middleName: middleName,
                address: address,
				email: email,
				password: password1, 
				mobileNo: mobileNo
			})
		}).then(response => response.json())
		.then(dataNakaJSON => {
            if (dataNakaJSON.email) {
                return true
            } else {
                return false
            }
		})
        if (isRegistered) {
            setUserName('');
            setFirstName('');
            setLastName('');
            setMiddleName('');
            setAddress('');            
            setEmail('');
            setMobileNo('');
            setPassword1('');
            setPassword2('');

            await swal.fire({
                icon: 'success',
                title: 'Registered Successfully!',
                text: 'Please wait while redirecting.',
              })  
         window.location.href = "/login";  
        } else {
            await swal.fire({
                icon: 'error',
                title: 'Email/Username Exist',
                text: 'Please try different input',
              }) 
        }
     };
    return(
        
        <div>
            {
              user.id ?
              <Navigate to='/products'replace={true}/> 
              : 
              <>
              <AppNavBar/>
              <Container className='mt-5'>
                  {   
                      isAllowed ?    
                      <h1 className='text-center text-success'>You May Now Register!</h1>
                      :  
                      <h1 className='text-center'>Registration Form</h1>
                  }
  
             <Form onSubmit={e => registerUser(e)} id='registerForm' className='border p-5 registerForm mt-2' >
                   {/*  Username Field */}
                   <Form.Group>
                     <Form.Label className='text-white regForm'>Username</Form.Label>
                     <Form.Control 
                     className='formRegister shadow-lg'
                     type='text' 
                     placeholder='Enter your Username' 
                     required value={userName} 
                     onChange={event => {setUserName(event.target.value)}}>
                     </Form.Control>
                 </Form.Group>
                  {/* First Name Field */}
                 <Form.Group>
                     <Form.Label className='text-white'>First Name</Form.Label>
                     <Form.Control 
                     className='formRegister shadow-lg'
                     type='text' 
                     placeholder='Enter your first Name' 
                     required value={firstName} 
                     onChange={event => {setFirstName(event.target.value)}}>
  
                     </Form.Control>
                 </Form.Group>
                  {/* Last Name Field */}
                 <Form.Group>
                     <Form.Label className='text-white'>Last Name</Form.Label>
                     <Form.Control className='formRegister shadow-lg ' type='text'placeholder='Enter your last Name' required value={lastName} onChange={event => {setLastName(event.target.value)}}></Form.Control>
                 </Form.Group>
                  {/* MiddleName Field */}
                  <Form.Group>
                     <Form.Label className='text-white'>Middle Name</Form.Label>
                     <Form.Control className='formRegister shadow-lg' type='text'placeholder='Enter your Middle Name' required value={middleName} onChange={event => {setMiddleName(event.target.value)}}></Form.Control>
                 </Form.Group>
                  {/* Address Field */}
                  <Form.Group>
                     <Form.Label className='text-white'>Address</Form.Label>
                     <Form.Control className='formRegister shadow-lg' type='text'placeholder='Enter your last Name' required value={address} onChange={event => {setAddress(event.target.value)}}></Form.Control>
                 </Form.Group>
                  {/* Emaill address Field */}
                 <Form.Group>
                     <Form.Label className='text-white'>Email</Form.Label>
                     <Form.Control className='formRegister shadow-lg' type='email'placeholder='Enter your email' required value={email} onChange={event => {setEmail(event.target.value)}}></Form.Control>
                 </Form.Group>
                  {/* Mobile Field */}
                 <Form.Group>
                     <Form.Label className='text-white'>Mobile Number</Form.Label>
                     <Form.Control className='formRegister shadow-lg ' type='tel' pattern='[0]{1}[8-9]{1}[0-9]{9}' placeholder='Ex.09975110865' required value={mobileNo} onChange={event => {setMobileNo(event.target.value)}}></Form.Control>
                     {
                         isMobileValid?
                         <span className='text-success'>Mobile is Valid </span>
                         :
                         <span className='text-secondary'>Mobile No. 11 digits </span>
                     }
                 </Form.Group>   
                  {/* Password Field */}
                 <Form.Group>
                     <Form.Label className='text-white'>Password</Form.Label>
                     <Form.Control className='formRegister shadow-lg' type='password' placeholder='Enter your  password' required value={password1} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character" onChange={event => {setPassword1(event.target.value)}}></Form.Control>
                 </Form.Group>
                  {/* Confirm Password Field */}
                 <Form.Group>
                     <Form.Label className='text-white'>Confirm Password</Form.Label>
                     <Form.Control className='formRegister shadow-lg'type='password' placeholder='Confirm your  password'   required value={password2} pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"  onChange={event => {setPassword2(event.target.value)}}>
                     </Form.Control>
                     {
                         isMatched ?
                         <span className='text-success'>Password Matched </span>
                         :
                         <span className='text-warning'>Password don't match</span>
                     }
                 </Form.Group>
                  {/* Register Button */}
                  
                  {
                   isActive ?
                      <Button className='btn-block btn-dark' type='submit'>Register</Button>
                        :
                      <Button className='btn-block btn-dark' disabled>Register</Button>
                  }
             </Form>
          
         </Container>
         </>
            } 
          
        </div>
        
    );
};