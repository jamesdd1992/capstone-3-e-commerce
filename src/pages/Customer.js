import { useEffect, useState } from 'react';
import AppSideNavBar from '../components/AppSideNavBar';
import CustomerCard from '../components/CustomerCard';
import { Container } from 'react-bootstrap';
import UserContext from '../UserContext';
import { useContext } from 'react';
import { Table } from 'react-bootstrap';







export default function Customers(){
    
    let {user} = useContext(UserContext)
    
    const [userCollection, setUserCollection] = useState([]);
    useEffect(()=> {
        let token = localStorage.getItem('accessToken')
        fetch('https://capstone2-batch156.herokuapp.com/users/getallusers',{
        headers:{
            'Authorization' : `Bearer ${token}`
        }
    }).then(res => res.json()).then(convertedData => {
        console.log(convertedData)
        setUserCollection(convertedData.map(user => {
            return(
                <CustomerCard key={user._id}  userProp={user} />
                )
            }))  
        });
    },[]);
    
    return(
        <div>
        {
            user.isAdmin === true ? 
            <>
            <AppSideNavBar/>   
            
            <Container className='mb-5 pb-2 bg-light'>
            <h1 className='text-center mt-5'>CUSTOMER INFORMATION</h1>
            <Table striped bordered hover variant="dark">
            <thead>
            <tr>
            <th className='w-25'>First Name</th>
            <th  className='w-25'>Last Name</th>
            <th  className='w-25'>Email</th>
            <th  className='w-25'>Username</th>
            <th  className='w-25'>Role</th>
            <th  className='w-25'>OPTION</th>
            </tr>
            </thead>
            </Table>
            {userCollection}
            </Container>
            </>
            :
            window.location.href ="/admin"
        }
        </div>
        );
    }
    