import AppSideNavBar  from '../components/AppSideNavBar';
import AdminDashBoard from '../components/AdminDashBoard';
import UserContext from '../UserContext';
import { useContext } from 'react';

const data = {
    title: 'Welcome to the Admin Page',
    content: 'Oppurtunities for everyone,everywhere'
}

export default  function AdminHome(){
    let {user} = useContext(UserContext)
    
    return (
        <>
        {
            user.isAdmin !== false ? 
            <div>
            <AppSideNavBar/>
            <AdminDashBoard bannerData={data}/>
            </div>
            :
            window.location.href = "*" 
        }
        </>
        );
    };
    