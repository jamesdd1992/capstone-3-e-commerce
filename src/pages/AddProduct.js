
import AppSideNavBar from '../components/AppSideNavBar';
import {Form, Button, Container} from 'react-bootstrap'
import swal from 'sweetalert2'
import { useState } from 'react';


export default function AddProduct(){
    
    const [brand, setBrand]  = useState('');
    const [name, setName] = useState('');
    const [price, setPrice] = useState('');
    const [description, setDescription]= useState('');
    
    const addProd = async (eventSubmit) => {
        eventSubmit.preventDefault()
        let token = localStorage.getItem('accessToken');
        const isCreatedProduct = await fetch('https://capstone2-batch156.herokuapp.com/product/createproduct',{
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            name: name,
            brand: brand,
            price: price,
            description: description
        })
    }).then(response => response.json())
    .then(dataNakaJSON => {
        return true
    })
    if (isCreatedProduct) {
        setBrand('');
        setName('');            
        setPrice('');
        setDescription('');
        await swal.fire({
            icon: 'success',
            title: 'Successfully Add Product!',
            text: 'Please wait while redirecting.',
        })  
        
    } else {
        await swal.fire({
            icon: 'error',
            title: 'Something Went Wrong',
            text: 'Try Again Later',
        }) 
    }
    
};
return(
    <div>
    <AppSideNavBar/>
    <Container>
    <Form onSubmit={e => addProd(e)} className='addProduct p-5 mt-5'>
    <h1>Add Product</h1>
    {/* Product Field */}
    <Form.Group>
    <Form.Label>Product Name</Form.Label>
    <Form.Control type='text' required placeholder='Enter Product Name' value={name} onChange={event => {setName(event.target.value)}} ></Form.Control>
    </Form.Group>
    {/* Brand Field */}
    <Form.Group>
    <Form.Label>Brand Name</Form.Label>
    <Form.Control type='text' required placeholder='Enter Brand Name' value={brand} onChange={event => {setBrand(event.target.value)}} ></Form.Control>
    </Form.Group>
    {/* Price Field */}
    <Form.Group>
    <Form.Label>Price</Form.Label>
    <Form.Control type='number' required placeholder='Enter price' value={price} onChange={event => {setPrice(event.target.value)}}></Form.Control>
    </Form.Group>
    {/* Description Field */}
    <Form.Group>
    <Form.Label>Description</Form.Label>
    <Form.Control as='textarea' rows={5}  type='text' required placeholder='Enter Description' value={description}  onChange={event => {setDescription(event.target.value)}}></Form.Control>
    </Form.Group>
    <input className="form-control bg-transparent file"  type="file" id="formFile" ></input>
    <Button className='btn btn-dark  ml-2 mt-3' type='submit'>Submit</Button>
    <Button className='btn btn-dark  ml-2 mt-3' href="/admin">Cancel</Button>
    </Form>
    </Container>
    
    
    
    
    </div>
    
    );
}