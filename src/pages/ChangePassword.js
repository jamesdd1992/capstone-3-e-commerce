import ChangePasswordCard from "../components/ChangePasswordCard";
import AppSideNavBar from "../components/AppSideNavBar";
import UserContext from "../UserContext";
import { useContext } from "react";
import AppNavBar from "../components/AppNavBar";

export default function Customer(){
    let {user} = useContext(UserContext)
  
    return(
        user.isAdmin !== false ? 
        <>
        <AppSideNavBar/>
        <ChangePasswordCard/>
        </>
        :
        <>
        <AppNavBar/>
        <ChangePasswordCard/>
        </>
    
    )
}