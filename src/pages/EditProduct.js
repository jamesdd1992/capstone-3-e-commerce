
import AppSideNavBar from '../components/AppSideNavBar';
import { Button,Container,Form } from 'react-bootstrap'
import { useParams } from 'react-router-dom';
import swal from 'sweetalert2'
import { useState, useEffect } from 'react';
import UserContext from '../UserContext';
import { useContext } from 'react';



export default function ProductView (){
    let {user} = useContext(UserContext)
    
    const [productInfo, setProductInfo] = useState({
        name: null,
        description: null,
        price: null
    });
    const [name, setName] = useState('')
    const [price, setPrice] = useState('')
    const [description, setDescription] = useState('')
    
    const {id} = useParams()
    
    useEffect(() =>{
        fetch(`https://capstone2-batch156.herokuapp.com/product/${id}`)
        .then(res => res.json())
        .then(convertedData => {
            console.log(convertedData)
            setProductInfo({
                id: convertedData._id,
                name: convertedData.name,
                description: convertedData.description,
                price: convertedData.price
            })
        });
        
        
    },[id])
    
    
    const update = async (eventUpdate) =>{
        eventUpdate.preventDefault()
        let token = localStorage.getItem('accessToken')
        const isUpdated = await fetch(`https://capstone2-batch156.herokuapp.com/product/${id}`,{
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${token}`
        },
        body: JSON.stringify({
            name: name,
            price: price,
            description: description,
            
        })  
    }).then(res => res.json()).then(convertedData => {
        if (convertedData) {
            return true
        } else {
            return false
        }
    })
    if (isUpdated) {
        setName('');
        setPrice('');
        setDescription('');
        await swal.fire({
            position: 'top-end',
            icon: 'success',
            title: 'Update Product Successfully!',
            showConfirmButton: false,
            timer: 1500
        }) 
        window.location.href ='/admin-view-products';
        
    } else {
        swal.fire({
            icon: 'error',
            title: 'Something Went Wrong',
            text: 'Try Again Later',
        }) 
    } 
    
    
};
return(
    <>
    {
        user.isAdmin === true ? 
        <>
        <AppSideNavBar/>
        
        <Container >
        <Form className='p-5 editProduct border mt-5  ' onSubmit={event => update(event)} >
        <h1>EDIT:{productInfo.name}</h1>
        <Form.Group>
        <Form.Label>Name</Form.Label>
        <Form.Control type='text' placeholder='enter product name' value={name}
        onChange={event =>{setName(event.target.value)}} ></Form.Control>
        </Form.Group>
        <Form.Group>
        <Form.Label>Price</Form.Label>
        <Form.Control type='number' placeholder='enter price' value={price}
        onChange={event =>{setPrice( event.target.value)}}></Form.Control>
        </Form.Group>
        <Form.Group>
        <Form.Label>Description</Form.Label>
        <Form.Control  as="textarea" rows={5} type='text' placeholder='enter description'  value={description}  onChange={event =>{setDescription( event.target.value)}}></Form.Control>
        </Form.Group>
        <input className="form-control bg-transparent file"  type="file" id="formFile" ></input>
        <Button className='btn btn-dark ml-2 mt-3' type='submit'>Update</Button>
        <Button className='btn btn-dark ml-2 mt-3' href='/admin-view-products'>Cancel</Button>
        </Form>
        </Container></>
        :
        window.location.href ="/admin"
    }
    
    
    </>
    )
};