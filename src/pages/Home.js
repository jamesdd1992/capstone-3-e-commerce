import AppNavBar from '../components/AppNavBar';
import HomeBanner from './../components/HomeBanner'
import Highlights from './../components/Highlights'
import UserContext from '../UserContext';
import { useContext } from 'react';

const data = {
    title: 'NEW NIKE COLLECTIONS',
    content: 'Opening sale up to 40% off'
}

export default  function Home(){
    let {user} = useContext(UserContext)
    return (
        <div>
            {
        user.isAdmin !== true ? 
        <>
        <AppNavBar/>
        <HomeBanner bannerData={data}/>
        <Highlights/>
        </>
        :
        window.location.href ="/admin"
        }
       
        </div>
    );
};
