//this component will b e used as the heroe section of our page.

//responsivee
import {Row, Col, Container} from 'react-bootstrap';
//we will use default bootstrap utility classes to format the component
//create the fucntion that will describe the structure of the hero section
//class > reserve keyword
//react jsx className

export default function AdminDashBoard({bannerData}) {
    return(
        <Container>
        <div className='m-5 p-5 bg-light' >
        <Row className='p-5'>
            <Col className='text-center'>
            <h1> {bannerData.title}</h1>
                <p className='my-3'>{bannerData.content}</p>
                <a className='btn btn-dark' href='/sales'>VIEW SALES</a>  
            </Col>
          
        </Row>
        </div>
        </Container>
    );
};

