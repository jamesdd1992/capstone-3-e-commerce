
import {useEffect, useState} from 'react'
import {Form, Button, Container} from 'react-bootstrap'
import Swal from 'sweetalert2'


export default function Login(){
    
    const [oldPassword, setOldPassword] = useState('');
    const [newPassword, setNewPassword] = useState('');
    const [newPassword2, setNewPassword2] = useState('');
    const [userName, setUserName] = useState('');
    const [isValid, setIsValid] = useState([false]);
    
    
    useEffect(() =>{
        if (userName !== '' &&
        oldPassword !== '' && 
        newPassword !== '' &&
        newPassword2 !== ''  &&
        newPassword === newPassword2 ) {
            setIsValid(true)
            
        } else {
            setIsValid(false)
        }
    },[oldPassword, newPassword,newPassword2,userName,isValid])
    
    
    const changePassword = async (eventSubmit) => {
        eventSubmit.preventDefault();
        let token = localStorage.getItem('accessToken')
        const isPasswordChange = await   fetch('https://capstone2-batch156.herokuapp.com/users/changepass',{
        method:'POST',
        headers: {'Authorization' : `Bearer${token}`,
        'Content-type' : 'application/json'
    },
    body: JSON.stringify({
        userName:userName,
        password: oldPassword,
        newpassword: newPassword
    })
}).then(res => res.json()).then(convertedData => {
    if (convertedData) {
        return  true
    } else {
        return false
    }
})
if(isPasswordChange)
{
    Swal.fire(
        'Good job!',
        'Password Changed Successfully',
        'success',
        )
        setNewPassword('');
        setUserName('');
        setNewPassword2('');
        setOldPassword('')
    }else{
        Swal.fire(
            'Oh No!',
            'Please Check your Username Or Password',
            'error',
            )
        }
    };
    return(
        <div>
        <Container className='mt-2'>
        <h1 className='text-center'>Change Password</h1>
        <div className='border container mt-2' id='ChangePass'>
        <Form onSubmit={event => changePassword(event)} >
        {/* UserNameField */}
        <Form.Group className='mt-2'>
        <Form.Label>Username</Form.Label>
        <Form.Control className='formLogin' type='text' required placeholder='Enter your username' value={userName} onChange={event => {setUserName(event.target.value)}}></Form.Control>
        </Form.Group>
        {/* old Password Field */}
        <Form.Group className='mt-2'>
        <Form.Label>Old Password</Form.Label>
        <Form.Control className='formLogin' type='password' required placeholder='Enter your old password' value={oldPassword} onChange={event => {setOldPassword(event.target.value)}}></Form.Control>
        </Form.Group>
        
        {/* Password Field 1*/}
        <Form.Group className='mt-2'>
        <Form.Label>New Password</Form.Label>
        <Form.Control className='formLogin' type='password' required placeholder='Enter your new password' pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character" value={newPassword} onChange={event => {setNewPassword(event.target.value)}}></Form.Control>
        </Form.Group>
        {/* Password Field 2*/}
        <Form.Group className='mt-2'>
        <Form.Label>Confirm Password</Form.Label>
        <Form.Control className='formLogin'pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,}$" title="Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character"  type='password' required placeholder='Enter your new password'  value={newPassword2} onChange={event => {setNewPassword2(event.target.value)}}>
        </Form.Control>
        </Form.Group>
        {isValid ? 
            <Button className='btn btn-block  btn-dark ' type='submit'>Submit</Button>
            :
            <Button className='btn btn-block btn-secondary' type='submit' disabled>Submit</Button>
            
        }
        </Form>
        
        </div>
        </Container>
        </div>
        );
    }
    