//identify needed components
import { Card,  } from 'react-bootstrap';
import { Link } from 'react-router-dom';



export default function productCard({productProp}){
    console.log(productProp)
    return(
        <Card className='m-4 d-md-inline-flex d-sm-inline-flex cardForm '>
        <Card.Body className='overflow-hidden' > 
        <Card.Title>
        {productProp.name}
        </Card.Title>
        <Card.Img variant='top' src={`${productProp._id}.jpg`} className='mt-2' />
        <Card.Text >{productProp.description}</Card.Text>
        </Card.Body>
        <Card.Text className='pl-2'>  {`Price:\u20B1${productProp.price}`} </Card.Text>
        
        <Link to={`/product/view-product/${productProp._id}`}  className='btn btn-dark'>View Product</Link>
        </Card>
        )  
    };
    